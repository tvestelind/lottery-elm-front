FROM node:latest

RUN npm install -g --unsafe-perm elm elm-test
ENV ELM_ROOT /var/www/elm
RUN mkdir -p ELM_ROOT
WORKDIR $ELM_ROOT

COPY . .

CMD ["elm", "reactor"]