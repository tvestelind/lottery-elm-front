module Pages.Top exposing (Flags, Model, Msg, page)

import Browser
import Dict as D exposing (..)
import Element as E exposing (..)
import Element.Background as EBack exposing (..)
import Element.Border as EBord exposing (..)
import Element.Input as EI exposing (..)
import Element.Keyed as EK exposing (..)
import List as L exposing (..)
import List.Extra exposing (..)
import Maybe exposing (..)
import Maybe.Extra exposing (..)
import Page exposing (Document, Page)
import String exposing (..)


type alias Flags =
    ()


page : Page Flags Model Msg
page =
    Page.sandbox
        { init = init
        , update = update
        , view = view
        }


type alias Name =
    String


type alias Amount =
    Int


type alias Entry =
    { name : Maybe Name
    , amount : Maybe Amount
    }


type alias Model =
    { entries : List Entry
    }


init : Model
init =
    { entries = [ { name = Nothing, amount = Nothing } ]
    }


type alias NewName =
    Name


type alias NewAmount =
    Amount


type alias Index =
    Int


type Msg
    = EditPerson Index NewName
    | AddEmptyEntry
    | RemoveEntry Index
    | NewAmount Index NewAmount
    | IncorrectAmount String


update : Msg -> Model -> Model
update msg model =
    case msg of
        EditPerson i n ->
            { model | entries = updateAt i (\e -> { e | name = Just n }) model.entries }

        AddEmptyEntry ->
            { model | entries = model.entries ++ [ { name = Nothing, amount = Nothing } ] }

        RemoveEntry i ->
            { model | entries = removeAt i model.entries }

        NewAmount i a ->
            { model | entries = updateAt i (\e -> { e | amount = Just a }) model.entries }

        IncorrectAmount _ ->
            model


view : Model -> Document Msg
view model =
    { title = "Run a lottery"
    , body =
        [ E.column [ E.centerX, E.centerY ]
            [ viewEntries model.entries
            , buttonRow
            ]
        ]
    }


buttonRow : Element Msg
buttonRow =
    E.row [ alignRight ]
        [ EI.button
            [ EBack.color (rgb255 192 192 192)
            , rounded 2
            , solid
            , EBord.width 2
            , EBord.color (rgb255 102 102 102)
            , E.width (px 70)
            , E.height (px 40)
            ]
            { onPress = Just AddEmptyEntry, label = E.el [ centerX ] (E.text "+") }
        , EI.button
            [ EBack.color (rgb255 192 192 192)
            , rounded 2
            , solid
            , EBord.width 2
            , EBord.color (rgb255 102 102 102)
            , E.width (px 150)
            , E.height (px 40)
            ]
            { onPress = Nothing, label = E.el [ centerX ] (E.text "Submit") }
        ]


viewEntries : List Entry -> Element Msg
viewEntries ee =
    EK.column [] (L.map (\( i, e ) -> ( fromInt i, e )) (viewKeyedEntries ee))


viewKeyedEntries : List Entry -> List ( Index, Element Msg )
viewKeyedEntries =
    indexedMap (\i e -> ( i, viewEntry i e ))


viewEntry : Index -> Entry -> Element Msg
viewEntry i e =
    E.row []
        [ EI.text []
            { onChange = EditPerson i
            , text = withDefault "" e.name
            , placeholder = Just (EI.placeholder [] (E.text "Name"))
            , label = labelHidden ("input_name_" ++ fromInt i)
            }
        , EI.text []
            { onChange = \s -> toInt s |> unwrap (IncorrectAmount s) (NewAmount i)
            , text = unwrap "" (\a -> fromInt a) e.amount
            , placeholder = Just (EI.placeholder [] (E.text "Amount"))
            , label = labelHidden ("input_amount_" ++ fromInt i)
            }
        , EI.button
            [ EBack.color (rgb255 192 192 192)
            , rounded 2
            , solid
            , EBord.width 2
            , EBord.color (rgb255 102 102 102)
            , E.width (px 45)
            , E.height (px 45)
            ]
            { onPress = Just (RemoveEntry i), label = E.el [ centerX ] (E.text "-") }
        ]
